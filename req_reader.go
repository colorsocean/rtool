package rtool

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"reflect"
	"strings"

	"gopkg.in/validator.v2"

	"bitbucket.org/colorsocean/convert"
	"bitbucket.org/colorsocean/response"
)

// todo: [V] Accept structs
// todo: [ ] Accept strings
// todo: [ ] Accept byte slices
// todo: [ ] Accept array (same name) fields

const (
	hdrXRW = "X-Requested-With"
	xrwXHR = "XMLHttpRequest"

	hdrCT  = "Content-Type"
	ctForm = "application/x-www-form-urlencoded"
	ctMFD  = "multipart/form-data"
	ctJSON = "application/json"
)

type Options struct {
	Out      interface{}
	Validate bool
	Response response.Response

	W http.ResponseWriter
	Q *http.Request
}

type Report struct {
	XHR             bool
	Post, Get       bool
	JSON            bool
	Form, Multipart bool
}

type structExpansion map[string]reflect.Value

func Read(o Options) (r Report, err error) {
	if o.Q.Header.Get(hdrXRW) == xrwXHR {
		r.XHR = true
	}
	conv := convert.New(convert.DefaultInit)

	expandStruct := func(v interface{}) (exp structExpansion) {
		exp = structExpansion{}

		vval := reflect.ValueOf(v)
		for vval.Kind() == reflect.Ptr {
			vval = vval.Elem()
		}
		vtype := reflect.TypeOf(o.Out)
		for vtype.Kind() == reflect.Ptr {
			vtype = vtype.Elem()
		}

		for i := 0; i < vval.NumField(); i++ {
			exp[strings.ToLower(vtype.Field(i).Name)] = vval.Field(i)
		}

		return
	}

	var exp structExpansion

	parseValues := func(values url.Values) {
		exp = expandStruct(o.Out)

		for name, vals := range values {
			name = strings.ToLower(name)

			if len(vals) > 0 && vals[0] != "" {
				val := vals[0]

				if out, ok := exp[name]; ok {
					outT := out.Type()
					if outT.Kind() == reflect.Ptr && outT.Elem().Kind() == reflect.Struct {
						fmt.Println("STRUCT PTR")
						out.Set(reflect.New(outT.Elem()))
						fmt.Println("STRUCT PTR OKAY")
						err = json.Unmarshal([]byte(val), out.Interface())
						fmt.Println("STRUCT PTR OKAY 2", err)
					} else if outT.Kind() == reflect.Struct {
						fmt.Println("STRUCT")
						newOut := reflect.New(outT)
						err = json.Unmarshal([]byte(val), newOut.Interface())
						out.Set(newOut.Elem())
					} else {
						err = conv.Do(out, val)
					}
					if err != nil {
						if o.Response != nil {
							o.Response.Error(response.Error{
								Module: "validation",
								Desc:   err.Error(),
								Target: name,
							})
						} else {
							return
						}
					}
				}

			}

		}
	}

	handleFiles := func(file map[string][]*multipart.FileHeader) {
		if exp == nil {
			exp = expandStruct(o.Out)
		}

		for formField, files := range file {
			formField = strings.ToLower(formField)
			if out, ok := exp[formField]; ok && len(files) > 0 {
				switch out.Interface().(type) {
				case []byte, string:
					f, err := files[0].Open()
					if err != nil {
						panic(err)
					}
					defer f.Close()
					data, err := ioutil.ReadAll(f)
					if err != nil {
						panic(err)
					}
					out.Set(reflect.ValueOf(data).Convert(out.Type()))
				case *multipart.FileHeader:
					out.Set(reflect.ValueOf(files[0]))
				case []*multipart.FileHeader:
					out.Set(reflect.ValueOf(files))
				}
			}
		}
	}

	switch {
	case strings.HasPrefix(o.Q.Header.Get(hdrCT), ctMFD):
		r.Form = true
		r.Multipart = true
		err = o.Q.ParseMultipartForm(1 * 1024 * 1024)
		if err != nil {
			panic(err)
		}

		switch o.Q.Method {
		case "POST":
			r.Post = true
			parseValues(o.Q.MultipartForm.Value)
			handleFiles(o.Q.MultipartForm.File)
		default:
			return r, errors.New("Unsupported method")
		}
	case strings.HasPrefix(o.Q.Header.Get(hdrCT), ctForm):
		r.Form = true
		err = o.Q.ParseForm()
		if err != nil {
			panic(err)
		}

		switch o.Q.Method {
		case "POST":
			r.Post = true
			parseValues(o.Q.Form)
		default:
			return r, errors.New("Unsupported method")
		}
	default:
		switch o.Q.Method {
		case "GET":
			r.Get = true
			parseValues(o.Q.URL.Query())
		default:
			return r, errors.New("Unsupported Content-Type")
		}

	case strings.HasPrefix(o.Q.Header.Get(hdrCT), ctJSON):
		r.JSON = true

		switch o.Q.Method {
		case "POST":
			r.Post = true

			err = ReadJSONBody(o.Out, o.Q)
			if err != nil {
				return
			}
		default:
			return r, errors.New("Unsupported method")
		}
	}

	if o.Validate {
		verr := validator.Validate(o.Out)
		if verr != nil {
			if o.Response != nil {
				verrs := verr.(validator.ErrorMap)
				for k, v := range verrs {
					o.Response.Error(response.Error{
						Module: "validation",
						Desc:   v.Error(),
						Target: k,
					})
				}
			} else {
				err = verr
			}
		}
	}

	return
}

func ReadJSONBody(v interface{}, q *http.Request) error {
	var buf = bytes.NewBuffer([]byte{})

	_, err := buf.ReadFrom(q.Body)
	if err != nil {
		return err
	}
	defer q.Body.Close()

	if unmrshlr, ok := v.(json.Unmarshaler); ok {
		err = unmrshlr.UnmarshalJSON(buf.Bytes())
	} else {
		err = json.Unmarshal(buf.Bytes(), v)
	}
	if err != nil {
		return err
	}

	return nil
}
