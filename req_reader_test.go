package rtool_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/textproto"
	"testing"

	"bitbucket.org/colorsocean/rtool"
)

func TestMultipartAndJSON(t *testing.T) {
	ferror := func(err error) {
		if err != nil {
			t.Fatal(err)
			t.FailNow()
		}
	}

	buf := bytes.NewBuffer([]byte{})
	mprt := multipart.NewWriter(buf)

	{ //> Setup parts
		//> Part 1 => File
		part, err := mprt.CreatePart(textproto.MIMEHeader{
			"Content-Disposition": []string{`form-data; name="file"; filename="file.bin"`},
			"Content-Type":        []string{`application/octet-stream`},
		})
		ferror(err)
		_, err = part.Write([]byte(`file content`))
		ferror(err)

		//> Part 2 => JSON object
		part, err = mprt.CreatePart(textproto.MIMEHeader{
			"Content-Disposition": []string{`form-data; name="object"`},
			"Content-Type":        []string{`application/json`},
		})
		ferror(err)
		_, err = part.Write([]byte(`{"string":"hello", "integer":2}`))
		ferror(err)

		//> Part 3 => int
		part, err = mprt.CreatePart(textproto.MIMEHeader{
			"Content-Disposition": []string{`form-data; name="one"`},
		})
		ferror(err)
		_, err = part.Write([]byte(`1`))
		ferror(err)

		//> Part 3 => string
		part, err = mprt.CreatePart(textproto.MIMEHeader{
			"Content-Disposition": []string{`form-data; name="two"`},
		})
		ferror(err)
		_, err = part.Write([]byte(`1`))
		ferror(err)

		ferror(mprt.Close())
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, q *http.Request) {
		type Object struct {
			Integer int
			String  string
		}

		type Request struct {
			Object *Object

			File *multipart.FileHeader

			One int
			Two string
		}

		var req Request
		_, err := rtool.Read(rtool.Options{Out: &req, Q: q, Validate: true})
		ferror(err)

		data, err := json.MarshalIndent(req, "", "  ")
		ferror(err)
		fmt.Println(string(data))
		file, err := req.File.Open()
		ferror(err)
		data, err = ioutil.ReadAll(file)
		fmt.Println("File:", string(data))
	}))
	defer ts.Close()

	q, err := http.NewRequest("POST", ts.URL, buf)
	ferror(err)
	q.Header.Set("Content-Type", `multipart/form-data; boundary=`+mprt.Boundary())
	_, err = http.DefaultClient.Do(q)
	ferror(err)
	defer q.Body.Close()

	data, err := ioutil.ReadAll(q.Body)
	ferror(err)
	fmt.Println(string(data))
}
